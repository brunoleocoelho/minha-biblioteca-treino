/**
 * Reducers update the state according to those actions.
 * https://redux.js.org/basics/reducers
 */

import { combineReducers } from 'redux'
import { BOOK_ACTIONS, USER_ACTIONS } from './actions'

const { ADD_BOOK, TOGGLE_BOOK, ADD_LIBRARY } = BOOK_ACTIONS
const { SET_USER } = USER_ACTIONS

/** Reducer slice for books */
const booksReducer = (state = [], action) => {
    switch (action.type) {
        case ADD_LIBRARY:
            return [...action.books]

        case ADD_BOOK:
            return [...state, {...action.book}]
    
        case TOGGLE_BOOK:
            return [...state.map(book => {
                if (book.id = action.id) {
                    return {...book, selected: !book.selected}
                }
                return book
            })]

        default:
            return state
    }
}

/** Reducer slice for the user logged in */
const userReducer = (state = {}, action) => {
    switch (action.type) {
        case SET_USER:
            return {...state, ...action.user}
    
        default:
            return state
    }
}

/** 
 * Main reducer for the app state 
 * It calls every separated reducer slice, according the situation.
 * This is called **reducer composition**, and it's the fundamental pattern of building Redux apps.
 * 
 * https://redux.js.org/basics/reducers#splitting-reducers
 */
const mainBookReducer = combineReducers({
    books: booksReducer,
    user: userReducer,
})

export default mainBookReducer




/* Equivalent call for mainReducer and initialState
const initialState = {
    books: [],
    selected: [],
    user: {}
}
const mainBookReducer = (state = initialState, action) => {
    return {
        books: booksReducer(state.books, action),
        selected: selectedReducer(state.selected, action),
        user: userReducer(state.user, action),
    }
} 
*/