/**[//]: # (
 * Actions represent the facts about “what happened”
 * 
 * _Ref.: https://redux.js.org/basics/actions_
 * )
 */

/** Actions types for books */
export const BOOK_ACTIONS = {
    ADD_LIBRARY: 'ADD_LIBRARY',
    ADD_BOOK: 'ADD_BOOK',
    TOGGLE_BOOK: 'TOGGLE_BOOK',

}
/** Actions types for user */
export const USER_ACTIONS = {
    SET_USER: 'SET_USER'
}


// ------ Functions (actions) ------
export function addLibrary (books) {
    return { type: BOOK_ACTIONS.ADD_LIBRARY, books }
}

export function addBook (book) {
    return { type: BOOK_ACTIONS.ADD_BOOK, book }
}

export function toggleBook (id) {
    return { type: BOOK_ACTIONS.TOGGLE_BOOK, id }
}

export function setUser (user) {
    return { type: USER_ACTIONS.SET_USER, user }
}