/**
 * Store is the object that brings them together. The store has the following responsibilities:
 * * Holds application state;
 * * Allows access to state via [getState()](https://redux.js.org/api/store#getState);
 * * Allows state to be updated via [dispatch(action)](https://redux.js.org/api/store#dispatch);
 * * Registers listeners via [subscribe(listener)](https://redux.js.org/api/store#subscribe);
 * * Handles unregistering of listeners via the function returned by [subscribe(listener)](https://redux.js.org/api/store#subscribe).
 * 
 * _Ref: https://redux.js.org/basics/store_
 */

import { createStore } from 'redux'
import mainBookReducer from './reducers'


/**
 * The Store is the object that brings actions and reducers together
 * 
 * https://redux.js.org/api/createstore
 */
const store = createStore(mainBookReducer)

export default store