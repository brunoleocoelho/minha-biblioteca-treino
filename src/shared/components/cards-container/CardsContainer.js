import React from 'react';
import './CardsContainer.css';

const CardsContainer = ({children, className}) => {

    const containerClassName = [
        'cards-container',
        className && className
    ].join(' ');

    return(
        <div className={containerClassName}>
            {children}
        </div>
    )
}

export default CardsContainer;
