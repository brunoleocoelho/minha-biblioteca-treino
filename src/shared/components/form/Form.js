import React from 'react'
import PropTypes from 'prop-types'
import Button from '../button/Button'
import './Form.css'

/** Componente para renderizar um select */
export const SelectField = ({label, nuloLabel='...', options=[], onChange, classNames=[], ...rest}) => (
    <>
        <label className={classNames.length ? classNames[0] : "label-select"} >{label}</label>
        <select 
            className={classNames.length ? classNames[1] :"select-itself" }
            onChange={onChange && onChange} 
            {...rest && rest}
        >
            <option value='' >...{nuloLabel}...</option>
            {options.length && options.map(
                (item, key) => (
                    <option className={classNames.length ? classNames[2] :"option-select"} key={key} value={item.value}>{item.label}</option>
                )
            )}

        </select>
    </>
)

/** Componente para renderizar input */
export const InputField = ({ label, type = 'text', name, icon, placeholder = '', required, onChange, className, ...rest }) => (
    <div className="wrapper-field">
        {label && <label className={className || "input-label"}>{label}</label>}
        <input className={className || "input-field"}
            key={name}
            name={name}
            type={type}
            placeholder={placeholder}
            required={required}
            onChange={onChange && onChange} 
            {...rest && rest} />

        {icon && ( <span className="input-icon"><i className={`fas fa fa-${icon}`}></i></span> )}
    </div>
)

/** Componente para renderizar um form, contendo inputs e um button submit */
const Form = ({title, data, onSubmit, buttonTitle='Enviar'}) => {
    return (
        <form className="content-form" /* method={method} */
            onSubmit={event => {
                event.preventDefault()
                onSubmit && onSubmit(event)
            }} >
            {title && <h3>{title}</h3>}
            {data.map((item, key) => (<InputField key={key} {...item} />))}

            <div className="wrapper-field">
                <Button title={buttonTitle} onClick={onSubmit && onSubmit} />
            </div>
        </form>
    )
}

Form.propTypes = {
    title: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({ 
        label: PropTypes.string, 
        name: PropTypes.string, 
        type: PropTypes.string, 
        icon: PropTypes.string, 
        placeholder: PropTypes.string, 
        required: PropTypes.bool, 
        onChange: PropTypes.func
    }))
}

export default Form
