import React from 'react';
import './Page.css';

const Page = ({children, className, ...rest}) => {
    const internalClassName = [
        'app-page',
        className || ''
    ].join(' ');

    return (
        <div className={internalClassName} {...rest} >
            {children}
        </div>
    );
}
export default Page;