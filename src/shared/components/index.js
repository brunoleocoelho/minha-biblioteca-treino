import Header from './header/Header'
import Footer from './footer/Footer'
import CardBook from './card-book/CardBook'
import CardsContainer from './cards-container/CardsContainer'
import Page from './page/Page'
import ContentWrapper from './content-wrapper/ContentWrapper';
import Form from './form/Form'
import Button from './button/Button'
import TableData from './table-data/TableData'

export {
    Header,
    Footer,
    CardBook,
    CardsContainer,
    Page,
    ContentWrapper,
    Form,
    Button,
    TableData
}
