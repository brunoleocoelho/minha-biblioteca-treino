import React from 'react'
import './CardBook.css'
import Button from '../button/Button'

function CardBook ({book, onClick}) {
    const internalClassName = [
        'card-book small',
        book.selected && 'selected'
    ].join(' ')

    return (
        <div className={internalClassName} >
            <div className="image-container" >
                <img className="card-image" src={book.image_url} alt={book.image_url} />
            </div>

            <div className="book-info-container">
                <h4 className="book-title" onClick={() => alert(`${book.title} \n${book.author} \n${book.category}`)} title={book.title} >
                    {book.title.substring(0, 29) + (book.title.length > 30 ? '...':'')}
                </h4>
                <p className="book-author">{book.author}</p>
                <span className="book-info">{book.category}</span>
                <span className="book-info">{book.publisher}</span>
                <span className="book-info">{`ISBN ${book.isbn}`}</span>
            </div>

            <div className="button-container">
                <Button title='Selecionar'
                    className="button-add" 
                    onClick={onClick} 
                    hintText={`Reservar '${book.title}'`}
                    icon='plus' />
                {/* <button className="button-add" onClick={onClick} title={`Reservar '${book.title}'`} >
                    <i className={'fa fa-fw fa-plus'}></i> 
                </button> */}
            </div>
        </div>
    )
}

export default CardBook
