import React from 'react'
import './Button.css'

const Button = ({ title, onClick, icon = 'check', hintText, className, disabled }) => {
    return (
        <button
            title={hintText}
            className={className || "button-style"}
            onClick={onClick && onClick}
            disabled={disabled}
        >
            <i className={`fas fa fa-${icon}`}></i> {title}
        </button>
    )
}

export default Button