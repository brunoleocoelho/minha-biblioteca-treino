import React from 'react'
import './Footer.css'

function Footer() {
    const year = new Date().getFullYear().toString()
    return (
        <div className="footer">
            <div className="wrap"> &copy; 
                {`${year} Biblioteca. Todos os direitos reservados a seus respectivos autores e editoras.`}
            </div>
        </div>
    )
}

export default Footer


