import React from 'react'
import './ContentWrapper.css'

const ContentWrapper = ({title, actions, children}) => (
    <div>
        <header className="header">
            <div className="title">
                <h2>{title}</h2>
            </div>
            <div className="actions">
                {actions || ''}
            </div>
        </header>
        <div className="">
            {children}
        </div>
    </div>
)

export default ContentWrapper
