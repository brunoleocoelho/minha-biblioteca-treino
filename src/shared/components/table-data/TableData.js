import React from 'react'
import './TableData.css'

const TableData = ({header, data}) => (
    <table style={{height: (data.length * 35) }} >
        <thead className="t-head">
           <tr>{header && header.map((col, key) => (<th key={key}>{col.label}</th>))}</tr>
        </thead>
        <tbody>
            {data && data.map((row, rowKey) =>(
                <tr key={rowKey} className={(rowKey % 2) ? 'odd-row' : 'even-row'} >
                    { header && header.map((item, key) => (<td key={key}>{row[item.name]}</td>)) }
                </tr>
            ))}
        </tbody>
    </table>
)

export default TableData
