import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

// MODELO header object
// const header = {
//   title: 'Biblioteca do Bruno',
//   items: [
//     {label:'Livros', path:'/', icon: 'book'},
//     {label: 'Autores', path:'/', icon: 'users'},
//     {label: 'Minha estante', path:'/', icon: 'bookmark'},
//   ]
// }

/** Menu header principal do topo da página */
class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isMenuHidden: true,
      items: [],
      title: ''
    }
  }
  
  componentDidMount() {
    
  }

  toogleMenu = show => {
    this.setState({isMenuHidden: show})
  }

  render() {
    const { items, title } = this.props.header
    // console.log(this.props)
    return (
      <div className="header-menu">
        
        <div className={(this.state.isMenuHidden ? "nav-hidden" : "navigation") + ' nav-border-space'}>
          <ul className="nav-list">
            {items && items.map((item, key) => {
                if (item) 
                  return (<Link className="nav-item link-button" to={item.path} key={key} >
                            <i className={'fa fa-fw fa-'+ item.icon}></i>
                            <span>{ item.label }</span>
                        </Link>)
            })}
          </ul>
        </div>

        <Link className="link-button" to="/">
          <div className="header-title">
            {/* <i className={`fa fa-fw fa-university`}></i> */}
              <img className="library-logo" src={require('./library.png')} alt={title} />
              <span>{ title || 'Biblioteca' }</span>
          </div>
        </Link>

        <button className="menu-button" onClick={()=> this.toogleMenu(!this.state.isMenuHidden)}>
          <i className={'fa fa-fw fa-bars'}></i>
        </button>

      </div>
    )
  }
}

export default Header
