
/** Serviço para manipulação do localstorage */
const CacheStorageApi = {
    /** Recupera informações do usuário localmente */
    getUserInfo: () => {
        const encodedInfo = window.localStorage.getItem('userInfo')
        const userInfo = encodedInfo ? JSON.parse(atob(encodedInfo)) : null
        return userInfo
    },

    /** Armazena informações do usuário localmente */
    setUserInfo: (userInfo) => {
        const encodedInfo = btoa(JSON.stringify(userInfo))
        window.localStorage.setItem('userInfo', encodedInfo)
    },

    /** Remove as informações do usuário localmente */
    cleanUserInfo: () => {
        window.localStorage.removeItem('userInfo')
    },

    /** Retorna os livros selecionados do localstorage */
    getSelectedBooks: () => {
        return JSON.parse(window.localStorage.getItem('selectedBooks'))
    },

    /** Armazena os livros selecionados */
    setSelectedBooks: (selected) => {
        window.localStorage.setItem('selectedBooks', JSON.stringify(selected))
    },

    /** Remove os livros selecionados */
    cleanSelectedBooks: () => {
        window.localStorage.removeItem('selectedBooks')
    },

    /** Recupera os livros 'beforeFilter' do localstorage */
    getBooksBeforeFiltered: () => {
        return JSON.parse(window.localStorage.getItem('beforeFilter'))
    },
    
    /** Armazena os livros 'beforeFilter' no localsotorage */
    setBooksBeforeFiltered: filteredBooks => {
        window.localStorage.setItem('beforeFilter', JSON.stringify(filteredBooks))
    },

    /** Remove do localstorage os livros 'beforeFilter' */
    cleanBooksBeforeFiltered: () => {
        window.localStorage.removeItem('beforeFilter')
    }
}

export default CacheStorageApi