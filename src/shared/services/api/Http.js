const baseUrl = 'http://localhost:3001/api'

const Http = {
    get: (endPoint, params={}) => {
        return fetch(baseUrl + endPoint + buildQueryString(params))
            .then(response => response.json())
    },
    post: (endpoint, postData) => {
        return fetch(baseUrl + endpoint, {
            method: 'POST', 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(postData)
        })
        .then(response => response.json())
    }
}

/** Retorna uma querystring formatada do objeto recebido */
const buildQueryString = (objParams={}) => {
    if (!objParams) return ''

    const paramKeys = Object.keys(objParams)
    if (paramKeys.length > 0) {
        const queryString = paramKeys.reduce((prev,curr,idx) => {
            return prev + ((idx === 0) ? '?' : '&') + curr + '=' + objParams[curr]
        },'')
        return queryString
    }
    return ''
}

export default Http