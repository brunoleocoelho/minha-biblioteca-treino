import Http from './Http'

/** Acessa os endpoint da API da biblioteca */
const BibliotecaApi = {
    /** Lista os livros disponíveis */
    listBooks: page => {
        const url = `/book/list${(page) ? `?page=${page}` : ``}`
        return Http.get(url)
            .then(response => response)
            .catch(err => [])
    },
    /** Busca por titulos de livros conforme o termo passado por parametro  */
    getSearchOfBooks: search => {
        const url = `/book/search?q=${search}`
        return Http.get(url)
            .then(response => response)
            .catch(err => [])
    },
    /** Envia a a reserva para api */
    sendReservation: (reservation) => {
        const url = `/reservation`
        return Http.post(url, {reservation})
            .then(response => response)
            .catch(err => [])
    }
}

export default BibliotecaApi