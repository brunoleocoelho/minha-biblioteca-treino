import Http from './Http'

/** Acessa os endpoint da API de clientes da biblioteca. 
 * Cada "User" do SPA é na verdade um "Customer" na API.
*/
const UserApi = {
    /** Faz a autenticação do usuário */
    login: userData => {
        return Http.post('/auth', {userData})
            .then(response => response)
            .catch(err => [])
    },
    /** Registra um novo usuário */
    newuser: userData => {
        return Http.post('/customer/new', {userData})
            .then(response => response)
            .catch(err => [])
    },

}

export default UserApi