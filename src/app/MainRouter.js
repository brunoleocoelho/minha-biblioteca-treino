import React from 'react';
import { Route } from 'react-router-dom';
import { 
  HomePage, 
  LoginPage,
  ReservationPage
} from '../pages'

const MainRouter = () => (
    <React.Fragment>
      <Route exact path="/" component={HomePage} />
      <Route path="/login" component={LoginPage} />
      <Route path="/logout" render={props => (<LoginPage logOut={true} {...props} />)} />
      <Route path="/reservation" render={props => (<ReservationPage {...props}/>)} />
    </React.Fragment>
);
export default MainRouter;
