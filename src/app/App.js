import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'
import MainRouter from './MainRouter';
import 'font-awesome/css/font-awesome.min.css';
import './App.css';

import { Header, Footer } from '../shared/components'
import { CacheStorageApi } from '../shared/services/api'

const header = {
  title: 'Biblioteca',
  items: [
    {label: 'Livros', path:'/', icon: 'book'},
    CacheStorageApi.getUserInfo() && {label: 'LogOut', path:'/', icon: 'power-off'},
  ]
}

/** Start point para as páginas */
class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Header header={header} />
          <MainRouter />
          <Footer />
        </BrowserRouter>
      </div>
    )
  }
}

export default App;
