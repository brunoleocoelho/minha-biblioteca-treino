import Home from './home/Home'
import Login from './login/Login'
import Reservation from './reservation/Reservation'

export {
    Home as HomePage,
    Login as LoginPage,
    Reservation as ReservationPage
}