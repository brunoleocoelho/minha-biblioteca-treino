import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Page, Form } from '../../shared/components'
import { CacheStorageApi, UserApi } from '../../shared/services/api'
import './Login.css'


/** Página para login de usuário */
class Login extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            form: {},
            isRegistered: true,
            userInfo: CacheStorageApi.getUserInfo(),
            logOut: (this.props.logOut || false),
            redirect: (this.props.location.state || null),
        }
    }

    componentDidMount() { }

    registerUser = event => {
        // const regEmail = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+\.([a-z]+)?$/i
        // if (regEmail.test()) { }
        const { form, redirect } = this.state
        const encoded = btoa(JSON.stringify(form))
        console.log(form)
        console.log(encoded)

        UserApi.newuser(encoded).then(resp =>{
            console.log(resp)
            if (resp.id) {
                CacheStorageApi.setUserInfo(resp)
                if (redirect)
                    this.props.history.push(redirect.redirect)
                else
                    this.props.history.push('/')
            }
            else {
                window.alert(resp.error || 'Problema ao registrar!')
            }
        })
    }

    doLogout = () => {
        CacheStorageApi.cleanUserInfo();
        return <Redirect to="/" />
    }

    doLogin = () => {
        const { form, redirect } = this.state
        const encoded = btoa(JSON.stringify(form))
        console.log(form)
        console.log(encoded)

        UserApi.login(encoded).then(resp =>{
            console.log(resp)
            if (resp.id) {
                CacheStorageApi.setUserInfo(resp)
                if (redirect)
                    this.props.history.push(redirect.redirect)
                else
                    this.props.history.push('/')
            }
            else {
                window.alert(resp.error || 'Algum problema com login!')
            }
        })
    }

    handleField = event => {
        const {name, value} = event.target
        this.setState({form: {...this.state.form, [name]: value}})
    }

    getDataFields = () => {
        return [
            { label: 'Email', name: 'email', type: 'email', icon: 'user', placeholder: 'exemplo@email.com', required: true, onChange: e => this.handleField(e) },
            { label: 'Senha', name: 'password', type: 'password', icon: 'key', placeholder: '********', required: true, onChange: e => this.handleField(e) },
        ]
    }

    getRegisterFileds = () => {
        return [
            { label: 'Nome', name: 'name', type: 'text', icon: 'user', placeholder: 'digite seu nome aqui', required: true, onChange: e => this.handleField(e) },
            { label: 'Email', name: 'email', type: 'email', icon: 'envelope-square', placeholder: 'exemplo@email.com', required: true, onChange: e => this.handleField(e) },
            { label: 'Senha', name: 'password', type: 'password', icon: 'key', placeholder: '********', required: true, onChange: e => this.handleField(e) },
        ]
    }

    toggleForm = () => {
        console.log(this.state.isRegistered);
        this.setState({isRegistered: !this.state.isRegistered, form:{}})
    }

    render() {
        if (this.state.logOut)
            this.doLogout();
        
        else if (this.state.userInfo) 
            return <Redirect to='/' />

        return (
            <Page className="Login">
                {this.state.isRegistered
                    ? (<Form 
                        data={this.getDataFields()}
                        title="Login de usuário"
                        onSubmit={() => this.doLogin()}
                        buttonTitle='Entrar' />
                    )
                    : (<Form 
                        data={this.getRegisterFileds()}
                        title="Cadastro de usuário"
                        onSubmit={() => this.registerUser()}
                        buttonTitle='Regirstrar' />
                    )
                }
                { (!this.state.form.email && !this.state.form.name) 
                    && (<div className="cadastro">
                            <p onClick={() => this.toggleForm()}>
                                {this.state.isRegistered ? 'Não tenho cadastro.' : 'Já sou cadastrado!'}
                            </p>
                        </div>
                    )
                }
            </Page>
        )
    }
}

export default Login;
