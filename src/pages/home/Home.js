import React, { Component } from 'react'
import InfiniteScroll from 'react-infinite-scroller'
import { Page, ContentWrapper, CardsContainer, CardBook, Button } from '../../shared/components'
import { InputField, SelectField } from '../../shared/components/form/Form'
import { BibliotecaApi, CacheStorageApi } from '../../shared/services/api'
import store from '../../shared/store/store'
import { addLibrary } from '../../shared/store/actions'
import './Home.css'

/** Página inicial */
class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            books: [],
            hasNext: false,
            startPage: 1,
            isSearched: false,
            searchInput: '',
            filteredId: '',
            categories: [],
            selected: [], 
            filtered: []
        }
    }

    componentDidMount() {
        this.loadBooks()
    }

    /** Carrega os livros da API, obedecendo paginação */
    loadBooks = (page) => {
        let { books, isSearched } = this.state
        const unsubscribe = store.subscribe(() => console.log(store.getState()))
        if (isSearched) books = []
        BibliotecaApi.listBooks(page || 1).then(response => {
            if (response.books) {
                let moreBooks = [...books, ...response.books].map(livro => ({...livro, key:livro.id, selected:false}))
                let selected = CacheStorageApi.getSelectedBooks() || []
                let allBooks = []
                moreBooks.forEach(elem => {
                    allBooks.push({...elem, ...(selected.find(item => item.key === elem.key))})
                })
                let booksFull = [...allBooks].sort((a,b) => (a.title < b.title) ? -1 : (a.title > b.title) ? 1 : 0)

                store.dispatch(addLibrary(booksFull))
                unsubscribe()
                
                this.setState({
                    books: booksFull,
                    hasNext: response.hasNext,
                    startPage: page,
                    categories: this.defineCategoryOptions(allBooks),
                    selected
                })
            }
        })
    }

    /** Cria o array de seleção de livros, baseado na lista de livros na tela */
    defineCategoryOptions = (books) => {
        const reduced = books.reduce((prevObj, curr) => {
            const categ = curr.categoryId
            return {...prevObj, [categ]:  curr.category}
        },{})
        const categories = Object.keys(reduced).sort().map(elem => ({
            value: elem,
            label: reduced[elem]
        }))
        return categories
    }

    /** Atua na seleção de um livro */
    selectBook = (item) => {
        item.selected = !item.selected
        this.setState({ books: [...this.state.books], selected: this.state.books.filter(x => x.selected) },
            () => CacheStorageApi.setSelectedBooks(this.state.selected)
        )
    }

    /** Atua no input de busca de livros */
    handleInputSearch = event => {
        this.setState({ searchInput: event.target.value })
    }

    /** Atua na filtragem de livros */
    handleSelectFilter = event => {
        const filteredId = event.target.value
        if (filteredId.length === 0) {
            this.setState({filtered: [], filteredId})
        }

        const filtered = this.state.books.filter(book => book.categoryId === filteredId)
        this.setState({filtered, filteredId })
    }

    /** Atua na busca de livros na API */
    searchBook = () => {
        const { searchInput, isSearched } = this.state
        if (!isSearched) {
            if (searchInput.length >= 3) {
                BibliotecaApi.getSearchOfBooks(searchInput).then(response => {
                    this.setState({ books: [...response.books], isSearched: true })
                })
            }
        }
        else {
            this.setState({ searchInput: '', pageStart: 1, isSearched: false, books: [] }, () => {
                this.loadBooks()
            })
        }
    }

    /** Atua no direcionamento para reserva dos livros */
    doReservation = () => {
        const userInfo = CacheStorageApi.getUserInfo()
        if (userInfo) {
            this.props.history.push({
                pathname: '/reservation',
                state: { selected: this.state.selected }
            });
        }
        else {
            this.props.history.push({
                pathname: '/login',
                state: { redirect: '/reservation' }
            });
        }
    }

    render() {
        const { selected, searchInput, isSearched, categories, filteredId, filtered } = this.state
        const books = store.getState().books
        console.log('books', books)
        return (
            <InfiniteScroll
                pageStart={this.state.startPage}
                loadMore={this.loadBooks}
                hasMore={this.state.hasNext}
            >
                <Page className="Home">
                    
                    <ContentWrapper
                        title="Livros disponíveis"
                        actions={
                            <React.Fragment>
                                <div className="interacao-container">
                                    <Button title={'Reservar Livros'}
                                        className='button-action'
                                        onClick={() => this.doReservation()} 
                                        icon='hourglass'
                                        disabled={!selected.length} />
                                </div>
                                <div className="interacao-container">
                                    <form className="form-search" onSubmit={event => {event.preventDefault(); this.searchBook();}}>
                                        <InputField type='text'
                                            className='input-search'
                                            placeholder='Buscar livro...'
                                            value={searchInput} 
                                            onChange={event => this.handleInputSearch(event)} />
                                        <Button title=''
                                            className="button-action"
                                            onClick={() => this.searchBook()} 
                                            icon={isSearched ? 'close':'search'} />
                                    </form>
                                </div>
                                <div className="interacao-container">
                                    <SelectField label='Filtro categorias' nuloLabel='Todas'
                                        classNames={['label-category','select-category','option-category']}
                                        onChange={event => this.handleSelectFilter(event)} 
                                        value={filteredId}
                                        options={categories} />
                                </div>
                            </React.Fragment>
                        }
                    >
                        <CardsContainer>
                            {books.length
                                ? ( 
                                    filtered.length
                                    ? filtered.map((book) => (
                                        <CardBook key={book.id}
                                            book={book}
                                            onClick={() => this.selectBook(book)} />
                                    ))
                                    : books.map((book) => (
                                        <CardBook key={book.id}
                                            book={book}
                                            onClick={() => this.selectBook(book)} />
                                    ))
                                )
                                : <div style={{fontSize:30, margin:'auto', bottom:'50%', color:'#561212'}} >
                                    <i className="fa fa-fw fa-smile-o"></i>
                                    Ops! Nenhum resultado!
                                </div>
                            }
                        </CardsContainer>

                    </ContentWrapper>
                </Page>
            </InfiniteScroll>
        )
    }
}

export default Home;