import React, { Component } from 'react';
import { Page, ContentWrapper, Form, Button, TableData } from '../../shared/components'
import { InputField } from '../../shared/components/form/Form';
import './Reservation.css'
import { BibliotecaApi, CacheStorageApi } from '../../shared/services/api';

class Reservation extends Component {
    state = {
        reservation: {},
        selected: this.props.location.state.selected || []
    }

    tableHeaders = () => ([
        {label:'Id', name:'id'},
        {label:'Titulo', name:'title'},
        {label:'Autor', name:'author'},
        {label:'Categoria', name:'category'},
        {label:'Editora', name:'publisher'},
        {label:'ISBN', name:'isbn'},
    ])

    convertDate = (stringYMDDate) => {
        let arrDt = stringYMDDate.split('-')
        return new Date(arrDt[0], arrDt[1]-1, arrDt[2])
    }

    validateData = (reservation) => {
        let erros = []
        if (reservation) {
            if (!reservation.dateStart || !reservation.dateEnd) {
                erros.push('Data inicio e fim são obrigatórias')
            }            
            else {
                let now = new Date()
                let dt1 = this.convertDate(reservation.dateStart)
                let dt2 = this.convertDate(reservation.dateEnd)
                let prevDate = (dt1.setHours(0,0,0,0) < now.setHours(0,0,0,0) || dt2.setHours(0,0,0,0) < now.setHours(0,0,0,0))
                let dtRangeOk = dt1 < dt2
                if (prevDate) {
                    erros.push('Datas não podem ser anteriores a hoje.')
                }
                if (!dtRangeOk) {
                    erros.push('Data inicio maior ou igual que data fim.')
                }
            }
        }
        else {
            erros.push('Há erros no formulário de reserva! Verifique!')
        }

        return erros;
    }

    saveReservation = (event) => {
        event.preventDefault()
        const { reservation, selected } = this.state

        let erros = this.validateData(reservation)
        if (erros.length) {
            let aviso = erros.map( err => err).join(`\n`)
            window.alert('ERRO: ' + aviso)
        }
        else {
            reservation.items = selected
            reservation.user = CacheStorageApi.getUserInfo() || {id: '000004'}
            BibliotecaApi.sendReservation(reservation).then( response => {
                if (response.id) {
                    window.alert(`Reserva feita sob numero: ${response.id}`)
                    CacheStorageApi.cleanSelectedBooks()
                    this.props.history.push('/')
                }
                else {
                    window.alert('Ocorreu um erro ao salvar a reserva!')
                }
            })
        }
    }

    handleField = event => {
        const {name, value} = event.target
        this.setState({reservation: {...this.state.reservation, [name]: value}})
    }
    
    render() {
        const { selected } = this.state
        return (
            <Page className="Reservation">
                <ContentWrapper
                 title="Reserva"
                 actions={''} 
                >
                    <form className="reserva-form" onSubmit={() => {}} >
                        <InputField 
                            className="" 
                            name="dateStart" 
                            label="Inicio" 
                            key="dateStart"
                            type="date" 
                            required
                            onChange={(e) => this.handleField(e)} required />
                        <InputField 
                            className="" 
                            name="dateEnd" 
                            label="Fim" 
                            key="dateEnd"
                            type="date" 
                            required
                            onChange={(e) => this.handleField(e)} required  />
                        <Button title="Salvar Reserva" icon="check" onClick={e => this.saveReservation(e)} />
                    </form>

                    <div className="table-selected">
                        <TableData data={selected || []} header={this.tableHeaders()} />
                    </div>

                </ContentWrapper>
            </Page>
        )
    }
}

export default Reservation;
