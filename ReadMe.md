### Biblioteca de livros

Parte do processo seletivo para um time de desenvolvedores relacionado aos conceitos de React, Nodejs e javascript.


#### Descrição do projeto
Desenvolvedor um sistema de biblioteca de reservas de livros, com frontend React e backend Nodejs:

* A página inicial deve apresentar os livros disponíveis na biblioteca e deverá ser possível filtra-los por categoria. Não é necessário criar interfaces de cadastros de livros e categorias, eles deverão estar pré-cadastrados na base de dados.

* O usuário deve poder selecionar os livros do seu interesse e em seguida solicitar a reserva.

* Ao solicitar a reserva a aplicação deve apresentar uma tela de login para o usuário, bem como permitir o cadastro para os usuários que ainda não estão cadastrados. O cadastro do usuário deve ser bem simples, apenas nome, email e senha são suficientes.

* Após logar ou se cadastrar a aplicação deverá apresentar a tela para a reserva dos livros selecionados. Ela deverá apresentar os livros previamente selecionados e campos data para informar o período da reserva (dataInicio e dataFim).

* Ao efetuar a reserva o usuário receberá um protocolo de confirmação.
